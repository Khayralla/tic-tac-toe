﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Services;
using TicTacToe.Extensions;
using Microsoft.AspNetCore.Rewrite;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using TicTacToe.Options;
using TicTacToe.Filters;
using TicTacToe.ViewEngines;
using Microsoft.OpenApi.Models;
using TicTacToe.Data;
using Microsoft.EntityFrameworkCore;

namespace TicTacToe
{
    public class Startup
    {
        public IConfiguration _configuration { get; }
        public IHostEnvironment _hostEnvironment { get; }

        public Startup(IConfiguration configuration, IHostEnvironment hostEnvironment)
        {
            _configuration = configuration;
            _hostEnvironment = hostEnvironment;
        }

        public void ConfigureDevelopmentServices(IServiceCollection services)
        {
            ConfigureCommonServices(services);
        }

        public void ConfigureStagingServices(IServiceCollection services)
        {
            ConfigureCommonServices(services);
        }

        public void ConfigureCommonServices(IServiceCollection services)
        {
            services.AddLocalization(options => options.ResourcesPath = "Localizations");
            services.AddMvc()
                    .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix, options =>
                       options.ResourcesPath = "Localization")
                    .AddDataAnnotationsLocalization();
            services.AddSingleton<IUserService, UserService>();
            services.AddSingleton<IGameInvitationService, GameInvitationService>();
            services.Configure<EmailServiceOptions>(_configuration.GetSection("Email"));
            services.AddEmailService(_hostEnvironment, _configuration);
            services.AddRouting();
            services.AddSession(o => { o.IdleTimeout = TimeSpan.FromMinutes(30); });
            services.AddControllersWithViews(o => o.Filters.Add(typeof(DetectMobileFilter)));
            services.AddSingleton<IGameSessionService, GameSessionService>();
            services.AddTransient<IEmailTemplateRenderService, EmailTemplateRenderService>();
            services.AddTransient<IEmailViewEngine, EmailViewEngine>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSwaggerGen(options =>
                options.SwaggerDoc("v1", new OpenApiInfo {
                    Title = "Learning ASP.NET Core 3.0 Rest-API",
                    Version = "v1",
                    Description = "Demonstrating auto-generated API documentation",
                    Contact = new OpenApiContact
                    {
                        Name = "Kenneth Fukizi",
                        Email = "example@example.com"
                    },
                    License = new OpenApiLicense
                    {
                        Name = "MIT"
                    }
                }));

            var connectionString = _configuration.GetConnectionString("DefaultConnection");
            services.AddEntityFrameworkSqlServer()
                    .AddDbContext<GameDbContext>((serviceProvider, options) =>
                       options.UseSqlServer(connectionString)
                              .UseInternalServiceProvider(serviceProvider));

            var dbContextOptionsbuilder = new DbContextOptionsBuilder<GameDbContext>()
                                                .UseSqlServer(connectionString);
            services.AddSingleton(dbContextOptionsbuilder.Options);
        }

        public void ConfigureProductionServices(IServiceCollection services)
        {
            ConfigureCommonServices(services);
        }

        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                //app.UseStatusCodePages("text/plain", $"HTTP Error - Status Code:{0}");
            }

            var options = new RewriteOptions().AddRewrite("NewUser", "/UserRegistration/Index", false);
            app.UseRewriter(options);
            app.UseWebSockets();
            app.UseCommunicationMiddleware();
            //app.UseDirectoryBrowser();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();

            var supportedCultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
            var localizationOptions = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            };

            localizationOptions.RequestCultureProviders.Clear();
            localizationOptions.RequestCultureProviders.Add(new CultureProviderResolverService());
            app.UseRequestLocalization(localizationOptions);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                            name: "default",
                            pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
                endpoints.MapAreaControllerRoute(
                            name: "areas",
                            areaName: "Account",
                            pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
           {
               c.SwaggerEndpoint("/swagger/v1/swagger.json", "LEARNING ASP.NET CORE 3.0 V1");
               c.RoutePrefix = string.Empty;
           });

            using var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            scope.ServiceProvider.GetRequiredService<GameDbContext>().Database.Migrate();
        }
    }
}
