﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe.Options
{
    public class EmailServiceOptions
    {
        private string MailType { get; set; }

        private string MailServer { get; set; }

        private string MailPort { get; set; }

        private string UseSSL { get; set; }

        private string UserId { get; set; }

        private string Password { get; set; }

        private string RemoteServerAPI { get; set; }

        private string RemoteServerKey { get; set; }

        public string SendGrid_ApiKey { get; set; }

        public string SenderEmail { get; set; }

        public EmailServiceOptions ()
        {

        }

        public EmailServiceOptions(string mailType, string mailServer, string mailPort,
            string useSSL, string userId, string password, string remoteServerAPI,
            string remoteServerKey, string sendGrid_ApiKey, string senderEmail)
        {
            MailType = mailType;
            MailServer = mailServer;
            MailPort = mailPort;
            UseSSL = useSSL;
            UserId = userId;
            Password = password;
            RemoteServerAPI = remoteServerAPI;
            RemoteServerKey = remoteServerKey;
            SendGrid_ApiKey = sendGrid_ApiKey;
            SenderEmail = senderEmail;
        }

    }
}
