﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using TicTacToe.Models;
using TicTacToe.Services;
using Microsoft.Extensions.DependencyInjection;

namespace TicTacToe.Controllers
{
    public class UserRegistrationController : Controller
    {
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly ILogger<UserRegistrationController> _logger;

        public UserRegistrationController(IUserService userService, IEmailService emailService,
            ILogger<UserRegistrationController> logger)
        {
            _userService = userService;
            _emailService = emailService;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(UserModel userModel)
        {
            if (ModelState.IsValid)
            {
                await _userService.RegisterUser(userModel);
                return RedirectToAction(nameof(EmailConfirmation), new { userModel.Email });
            }

            return View(userModel);
        }

        [HttpGet]
        public async Task<IActionResult> EmailConfirmation(string email)
        {
            _logger.LogInformation($"##Start## Email confirmation process for { email}");
            var user = await _userService.GetUserByEmail(email);
            var urlAction = new UrlActionContext
            {
                Action = "ConfirmEmail",
                Controller = "UserRegistration",
                Values = new { email },
                Protocol = Request.Scheme,
                Host = Request.Host.ToString()
            };


            var userRegistrationEmail = new UserRegistrationEmailModel
            {
                DisplayName = $"{user.FirstName} {user.LastName}",
                Email = email,
                ActionUrl = Url.Action(urlAction)
            };

            var emailRenderService = Request.HttpContext.RequestServices.GetService<IEmailTemplateRenderService>();
            
            var message = await emailRenderService.RenderTemplate("EmailTemplates/UserRegistrationEmail",
            userRegistrationEmail, Request.Host.ToString());

            //var message = $"Thank you for your registration on our website, please click here to confirm your email " + $" { Url.Action(urlAction)}";
            try
            {
                await _emailService.SendEmail(email, "Tic-Tac-Toe Email Confirmation", message);
            }
            catch (Exception e)
            {
            }

            if (user?.IsEmailConfirmed == true)
                return RedirectToAction("Index", "GameInvitation", new { email });

            ViewBag.Email = email;
            user.IsEmailConfirmed = true;
            user.EmailConfirmationDate = DateTime.Now;
            user.FirstName = "Ali";
            user.LastName = "Athari2";
            await _userService.UpdateUser(user);
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string email)
        {
            var user = await _userService.GetUserByEmail(email);
            if(user != null)
            {
                user.IsEmailConfirmed = true;
                user.EmailConfirmationDate = DateTime.Now;
                await _userService.UpdateUser(user);

                return RedirectToAction("Index", "Home");
            }

            return BadRequest();
        }
    }
}
