﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Models;
using TicTacToe.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TicTacToe.Controllers
{
    [Produces("application/json")]
    [Route("restapi/v1/GameInvitation")]
    public class GameInvitationApiController : ControllerBase
    {
        private readonly IGameInvitationService _gameInvitationService;
        private readonly IUserService _userService;

        public GameInvitationApiController(IGameInvitationService gameInvitationService, IUserService userService)
        {
            _gameInvitationService = gameInvitationService;
            _userService = userService;
        }

        // GET: api/<GameInvitationApiController>
        [HttpGet]
        public async Task<IEnumerable<GameInvitationModel>> Get()
        {
            return await _gameInvitationService.All();
        }

        // GET api/<GameInvitationApiController>/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<GameInvitationModel> Get(Guid id)
        {
            return await _gameInvitationService.Get(id);
        }

        // POST api/<GameInvitationApiController>
        [HttpPost]
        public IActionResult Post([FromBody] GameInvitationModel invitation)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var invitedPlayer = _userService.GetUserByEmail(invitation.EmailTo);
            if (invitedPlayer == null)
                return BadRequest();

            _gameInvitationService.Add(invitation);
            return Ok();
        }

        // PUT api/<GameInvitationApiController>/5
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody] GameInvitationModel invitation)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var invitedPlayer = _userService.GetUserByEmail(invitation.EmailTo);
            if (invitedPlayer == null)
                return BadRequest();

            _gameInvitationService.Update(invitation);
            return Ok();
        }

        // DELETE api/<GameInvitationApiController>/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _gameInvitationService.Delete(id);
        }
    }
}
