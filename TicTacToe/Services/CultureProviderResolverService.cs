﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe.Services
{
    public class CultureProviderResolverService : RequestCultureProvider
    {
        private static readonly char[] _cookieSeparator = new[] { '|' };
        private static readonly string _culturePrefix = "c=";
        private static readonly string _uiCulturePrefix = "uic=";

        public override async Task<ProviderCultureResult>
            DetermineProviderCultureResult(HttpContext httpContext)
        {
            var pcr = await NullProviderCultureResult;

            if (GetCultureFromQueryString(httpContext, out string culture))
                pcr =  new ProviderCultureResult(culture, culture);
            else if (GetCultureFromCookie(httpContext, out culture))
                pcr = new ProviderCultureResult(culture, culture);
            else if (GetCultureFromSession(httpContext, out culture))
                pcr = new ProviderCultureResult(culture, culture);

            return  pcr;
        }

        private static bool GetCultureFromQueryString(HttpContext httpContext, out string culture)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            culture = null;

            if (httpContext.Request.QueryString.HasValue)
            {
                culture = httpContext.Request.Query["culture"];
            }

            return culture != null;
        }

        private static bool GetCultureFromCookie(HttpContext httpContext, out string culture)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException(nameof(httpContext));
            }

            culture = null;

            var cookie = httpContext.Request.Cookies["culture"];
            if (!string.IsNullOrEmpty(cookie))
            {
                culture = ParseCookieValue(cookie);
            }
            
            return !string.IsNullOrEmpty(culture);
        }

        public static string ParseCookieValue(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
                return null;

            var parts = value.Split(_cookieSeparator, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
                return null;

            var potentialCultureName = parts[0];
            var potentialUICultureName = parts[1];

            if (!potentialCultureName.StartsWith(_culturePrefix) ||
                !potentialUICultureName.StartsWith(_uiCulturePrefix))
                return null;

            var cultureName = potentialCultureName[_culturePrefix.Length..];
            var uiCultureName = potentialUICultureName[_uiCulturePrefix.Length..];

            if (cultureName == null && uiCultureName == null)
                return null;

            if (cultureName != null && uiCultureName == null)
                uiCultureName = cultureName;

            if (cultureName == null && uiCultureName != null)
                cultureName = uiCultureName;

            return cultureName;
        }

        private static bool GetCultureFromSession(HttpContext httpContext, out string culture)
        {
            culture = httpContext.Session.GetString("culture");
            return !string.IsNullOrEmpty(culture);
        }
    }
}
