﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Options;

namespace TicTacToe.Services
{
    public class SendGridEmailService : IEmailService
    {
        private readonly IOptions<EmailServiceOptions> _emailServiceOptions;
        private readonly ILogger<EmailService> _logger;

        public SendGridEmailService(IOptions<EmailServiceOptions> emaillServiceOptions,
                                    ILogger<EmailService> logger)
        {
            _emailServiceOptions = emaillServiceOptions;
            _logger = logger;
        }

        public Task SendEmail(string emailTo, string subject, string message)
        {
            _logger.LogInformation($"##Start## Sending email via SendGrid to {emailTo}");
            var apiKey = _emailServiceOptions.Value.SendGrid_ApiKey;
            var senderEmail = _emailServiceOptions.Value.SenderEmail;
            var client = new SendGridClient(apiKey);
            var fromEmail = new EmailAddress(senderEmail, senderEmail);

            var msg = new SendGridMessage
            {
                From = fromEmail,
                Subject = subject,
                PlainTextContent = message
            };

            msg.AddTo(new EmailAddress(senderEmail, "Tic-Tac-Toe"));
            client.SendEmailAsync(msg);

            return Task.CompletedTask;
        }

    }
}
