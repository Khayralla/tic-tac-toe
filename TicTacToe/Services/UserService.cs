﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using TicTacToe.Models;
using System.Linq;
using System.Collections.Generic;
using TicTacToe.Data;
using Microsoft.EntityFrameworkCore;

namespace TicTacToe.Services
{
    public class UserService : IUserService
    {
        private DbContextOptions<GameDbContext> _dbContextOptions;

        public UserService(DbContextOptions<GameDbContext> dbContextOptions)
        {
            _dbContextOptions = dbContextOptions;
        }

        public async Task<bool> RegisterUser(UserModel userModel)
        {
            using var database = new GameDbContext(_dbContextOptions);
            database.UserModels.Add(userModel);
            await database.SaveChangesAsync();
            return true;
        }

        public async Task<UserModel> GetUserByEmail(string email)
        {
            using var database = new GameDbContext(_dbContextOptions);
            return await database.UserModels.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task UpdateUser(UserModel userModel)
        {
            using var gameDbContext = new GameDbContext(_dbContextOptions);
            gameDbContext.Update(userModel);
            await gameDbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<UserModel>> GetTopUsers(int numberOfUsers)
        {
            using var gameDbContext = new GameDbContext(_dbContextOptions);
            return await gameDbContext.UserModels.OrderByDescending(x => x.Score).ToListAsync();
        }

        public Task<bool> IsOnline(string name)
        {
            return Task.FromResult(true);
        }

        public async Task<bool> IsUserExisting(string email)
        {
            using var gameDbContext = new GameDbContext(_dbContextOptions);
            return await gameDbContext.UserModels.AnyAsync(user => user.Email == email);
        }
    }
}
