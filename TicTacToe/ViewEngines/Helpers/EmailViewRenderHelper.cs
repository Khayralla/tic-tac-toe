﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace TicTacToe.ViewEngines.Helpers
{
    public class EmailViewRenderHelper
    {
        private IWebHostEnvironment _hostingEnvironment;
        private IConfiguration _configurationRoot;
        private IHttpContextAccessor _httpContextAccessor;

        public async Task<string> RenderTemplate<T>(string template, 
            IWebHostEnvironment hostingEnvironment, IConfiguration configurationRoot,
            IHttpContextAccessor httpContextAccessor, T model) where T : class
        {
            _hostingEnvironment = hostingEnvironment;
            _configurationRoot = configurationRoot;
            _httpContextAccessor = httpContextAccessor;
            var renderer = httpContextAccessor.HttpContext.RequestServices
                           .GetService<IEmailViewEngine>();
            return await renderer.RenderEmailToString<T>(template, model);
        }
    }
}
