﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TicTacToe.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }

        [Display(Name = "FirstName")]
        [Required(ErrorMessage = "FirstNameRequired"), DataType(DataType.Text)]
        public string FirstName { get; set; }
        
        [Display(Name = "LastName")]
        [Required(ErrorMessage = "LastNameRequired"), DataType(DataType.Text)]
        public string LastName { get; set; }

        [Display(Name ="Email")]
        [Required(ErrorMessage ="EmailRequired"), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name ="Password")]
        [Required(ErrorMessage = "PasswordRequired"), DataType(DataType.Password)]
        public string Password { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? EmailConfirmationDate { get; set; }

        public int Score { get; set; }
    }
}
