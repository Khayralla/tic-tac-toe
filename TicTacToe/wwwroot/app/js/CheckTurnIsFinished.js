﻿function EnableCheckTurnIsFinish() {
    interval = setInterval(() => { CheckTurnIsFininshed(); },
        2000);
}

function CheckTurnIsFininshed() {
    var port = document.location.port ? (":" + document.location.port) : "";
    var url = document.location.protocol + "//" + document.location.hostname + port +
        "/restapi/v1/GetSession/" + window.GameSessionId;

    $.get(url, function (data) {
        if (data.turnFinished === true && data.turnNumber >= window.TurnNumber) {
            CheckGameSessionIsFinnished();
            ChangeTurn(data);
        }
    });
}

function ChangeTurn(data) {
    var turn = data.turns[data.turnNumber - 1];
    DisplayImageTurn(turn);

    $("#activeUser").text(data.activeUser.email);
    if (data.activeUser.email !== window.EmailPlayer) {
        DisabledBoard(data.turnNumber);
    }
    else {
        EnableBoard(data.turnNumber);
    }
}

function DisableBoard(turnNumber) {
    var divBoard = $("#gameBoard");
    divBoard.hide();
    $("#divAlertWaitTurn").show();
    window.TurnNumber = turnNumber;
}

function EnableBoard(turnNumber) {
    var divBoard = $("#gameBoard");
    divBoard.Show();
    $("#divAlertWaitTurn").hide();
    window.TurnNumber = turnNumber;
}

function DisplayImageTurn(turn) {
    var c = $("#c_" + turn.y + "_" + turn.x);
    var css;

    if (turn.iconNumber === "1") {
        css = 'glyphicon glyphicon-unchecked';
    }
    else {
        caa = 'glyphicon glyphicon-remove-circle';
    }

    c.html('<i class="' + css + '"></i>');
}