﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Extensions;

namespace TicTacToe
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
                Host.CreateDefaultBuilder(args)
                    .ConfigureWebHostDefaults(webBuilder =>
                    {
                        webBuilder.UseStartup<Startup>();
                        webBuilder.CaptureStartupErrors(true);
                        webBuilder.PreferHostingUrls(true);
                        webBuilder.UseUrls("http://localhost:5000");
                        webBuilder.ConfigureLogging((hostingcontext, logging) =>
                           logging.AddLoggingConfiguration(hostingcontext.Configuration));
                    });
    }
}
